// Include libraries
#include <avr/io.h>
#define F_CPU 8000000
#include <util/delay.h>

// Define macros
#define pinModeOutput(directions, pin) (directions |= pin) // set port direction for output
#define pinModeInput(directions, pin) (directions &= ~pin) // set port direction for output
#define digitalWriteSet(port, pin) (port |= pin)					 // set port pin
#define digitalWriteClear(port, pin) (port &= ~pin)				 // clear port pin
#define delay(value) _delay_ms(value)											 // time delay
// Time related
#define clockCyclesPerMicrosecond() (F_CPU / 1000000L)
#define clockCyclesToMicroseconds(a) ((a) / clockCyclesPerMicrosecond())
#define microsecondsToClockCycles(a) ((a)*clockCyclesPerMicrosecond())

// Define constants
#define port_b PORTB
#define port_c PORTC
#define port_b_dir DDRB
#define port_c_dir DDRC
#define pin_led (1 << PC4)
#define pin_timer (1 << PB0)
#define pin_pump (1 << PB7)
#define pin_sol_1 (1 << PC7)
#define pin_sol_2 (1 << PB4)
#define pin_sol_3 (1 << PB5)
#define pin_sol_4 (1 << PB6)

// Define variables
unsigned long duration_high;
unsigned long duration_low;
float duty_cycle = 50;

void blink(unsigned long n)
{
	for (unsigned long i = 0; i < n; i++)
	{
		digitalWriteSet(port_c, pin_led);
		_delay_ms(200);
		digitalWriteClear(port_c, pin_led);
		_delay_ms(200);
	}
}

void inflate(int n)
{
	// Start the pump
	digitalWriteSet(port_b, pin_pump);
	// Open valves
	digitalWriteSet(port_b, pin_sol_4);
	digitalWriteSet(port_b, pin_sol_2);
	// Close valves
	digitalWriteClear(port_c, pin_sol_1);
	digitalWriteClear(port_b, pin_sol_3);
	for (unsigned long i = 0; i < n; i++)
	{
		_delay_ms(1);
	}
}

void deflate(unsigned long n)
{
	// Start the pump
	digitalWriteSet(port_b, pin_pump);
	// Open valves
	digitalWriteSet(port_b, pin_sol_3);
	digitalWriteSet(port_c, pin_sol_1);
	// Close valves
	digitalWriteClear(port_b, pin_sol_2);
	digitalWriteClear(port_b, pin_sol_4);
	for (unsigned long i = 0; i < n; i++) 	
	{
		_delay_ms(1);
	}
}

void hold(int n)
{
	// close
	digitalWriteClear(port_b, pin_pump);
	digitalWriteClear(port_c, pin_sol_1);
	digitalWriteClear(port_b, pin_sol_2);
	digitalWriteClear(port_b, pin_sol_3);
	digitalWriteClear(port_b, pin_sol_4);
	for (unsigned long i = 0; i < n; i++)
	{
		_delay_ms(1);
	}
}

void breathe(int time)
{
	// unsigned long loops = microsecondsToClockCycles(time);
	unsigned long inflate_loops = time / 100 * 62;
	unsigned long holdup_loops = time / 100 * 5;
	unsigned long holddown_loops = time / 100 * 5;
	unsigned long deflate_loops = time / 100 * 28;
	inflate((int)inflate_loops);
	hold((int)holdup_loops);
	deflate((int)deflate_loops);
	hold((int)holddown_loops);
}

// Measure pulses
unsigned long pulseIn(char pin, bool state, long unsigned int timeout)
{
	unsigned long max_loops = microsecondsToClockCycles(timeout);
	unsigned long average_high = 0;
	unsigned long average_low = 0;
	unsigned long total_high = 0;
	unsigned long total_low = 0;
	for (long unsigned int i = 0; i < max_loops; i++)
	{
		if (pin)
		{
			total_high++;
		}
		else
		{
			total_low++;
		}
	}
	average_high = total_high / max_loops;
	average_low = total_low / max_loops;
	if (state)
	{
		return average_high;
	}
	else
	{
		return average_low;
	}
}

// Main Function
int main(void)
{
	_delay_ms(200);

	// Initialize LED
	pinModeOutput(port_c_dir, pin_led);
	port_c_dir |= pin_led;
	port_c &= ~pin_led;
	digitalWriteClear(port_c, pin_led);
	// Initialize Timer pin
	pinModeInput(port_b_dir, pin_timer);
	port_b_dir |= pin_led;
	port_b &= ~pin_timer;
	digitalWriteClear(port_b, pin_timer);
	// Initialize Pump pin
	pinModeOutput(port_b_dir, pin_pump);
	port_b_dir |= pin_pump;
	port_b &= ~pin_pump;
	digitalWriteClear(port_b, pin_pump);
	// Initialize Solenoid 1 pin
	pinModeOutput(port_c_dir, pin_sol_1);
	port_c_dir |= pin_sol_1;
	port_c &= ~pin_sol_1;
	digitalWriteClear(port_c, pin_sol_1);
	// Initialize Solenoid 2 pin
	pinModeOutput(port_b_dir, pin_sol_2);
	port_b_dir |= pin_sol_2;
	port_b &= ~pin_sol_2;
	digitalWriteClear(port_b, pin_sol_2);
	// Initialize Solenoid 3 pin
	pinModeOutput(port_b_dir, pin_sol_3);
	port_b_dir |= pin_sol_3;
	port_b &= ~pin_sol_3;
	digitalWriteClear(port_b, pin_sol_3);
	// Initialize Solenoid 4 pin
	pinModeOutput(port_b_dir, pin_sol_4);
	port_b_dir |= pin_sol_4;
	port_b &= ~pin_sol_4;
	digitalWriteClear(port_b, pin_sol_4);

	// Loop
	while (1)
	{
		breathe(20000);
		// blink(4);
		// _delay_ms(1400);
	}
}

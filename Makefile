PROJECT=main
SOURCES=$(PROJECT).cpp

MMCU=atmega16u2
DFU_TARGET=$(MMCU)

F_CPU=8000000

CFLAGS=-mmcu=$(MMCU) -Wall -Os -DF_CPU=$(F_CPU)

$(PROJECT).out: $(SOURCES)
	avr-gcc $(CFLAGS) -I./ -o $(PROJECT).out $(SOURCES)

$(PROJECT).c.hex: $(PROJECT).out
	avr-objcopy -O ihex $(PROJECT).out $(PROJECT).c.hex;\
	avr-size --mcu=$(MMCU) --format=avr $(PROJECT).out

program: $(PROJECT).c.hex
	# dfu-programmer $(DFU_TARGET) flash $(PROJECT).c.hex
	sudo /usr/bin/avrdude -P usb -c avrisp2 -b 9600 -p ATmega16U2 -s -U flash:w:$(PROJECT).c.hex

erase:
	dfu-programmer $(DFU_TARGET) erase

erase-force:
	dfu-programmer $(DFU_TARGET) erase --force

clean: 
	rm $(PROJECT).c.hex $(PROJECT).out

fuse:
	sudo /usr/bin/avrdude -P usb -c avrisp2 -b 9600 -p ATmega16U2 -s -U lfuse:w:0xde:m